provider "google" {
  credentials = "${file("my-kubernetes-codelab-cred.json")}"
  project = var.project_id
}
data "google_client_config" "provider" {}
data "google_container_cluster" "ci_cd_cluster2" {
  name = "bhargav-123"
  location = "us-central1-c"
}
provider "helm" {
  kubernetes {
    token = data.google_client_config.provider.access_token
    host  = "https://${data.google_container_cluster.ci_cd_cluster2.endpoint}"
    cluster_ca_certificate = base64decode(
    data.google_container_cluster.ci_cd_cluster2.master_auth[0].cluster_ca_certificate
    )
  }
}

resource "helm_release" "test" {
  name       = "test"
  repository = "https://gitlab.com/api/v4/projects/29986787/packages/helm/stable"
  chart      = "helm_deploy_1"
  // repository_username = "Bhargav307"
  // repository_password = "xgV_EK5bbSvsh_jp2MRA"

  set {
    name  = "NodePort"
    value = "NodePort"
  }


    
}
